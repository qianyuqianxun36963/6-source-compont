<h1>spring-framework 源码下载与部署</h1>

<h2>一、下载源码</h2>

<p>首先到github上下载项目<a href="https://github.com/spring-projects/spring-framework">源码</a></p>

<h2>二、按步骤操作</h2>

<p>项目源码里面就有如何导入idea的指导文档：import-into-idea.md</p>

<p>按照文档操作即可：</p>

<p>1：预编译spring-oxm模块，官网说：Pre-compile `spring-oxm` with `./gradlew cleanIdea :spring-oxm:compileTestJava`， 即打开cmd，切换到spring-framework工程目录下，运行：gradlew cleanIdea :spring-oxm:compileTestJava。</p>

<p>2：开始导入工程,File-&gt;import project(找不到看常见问题)-&gt;选中spring-framework工程-&gt;import from external model-&gt;Gradle-&gt;finish经过一段时间项目导入成功</p>

<p>3：设置project sdk为1.8，右键project-&gt;open module settings-&gt;sdks-&gt;点+号新建jdk1.8,找到jdk1.8的路径，再选中project选项卡，设置project sdk为1.8，注意下面的project language level要选8.0</p>

<p>4：File-&gt;Project Structure-&gt;Modules删除掉spring-aspects模块。 原因是`spring-aspects` does not compile out of the box due to references to aspect types unknown to IDEA. See http://youtrack.jetbrains.com/issue/IDEA-64446 for details. In the meantime, the &#39;spring-aspects&#39;should be excluded from the overall project to avoid compilation errors.</p>

<h2>三、常见问题</h2>

<p>在导入gradle项目的时候，出现版本问题。&nbsp;<code>Error:No such property: GradleVersion for class: JetGradlePlugin</code></p>

<p>通过更换版本，可以解决：<a href="https://services.gradle.org/distributions/">gradle各版本下载路径</a></p>

<p>我原本安装的gradle版本是5.4.1版本的，说是高了，然后换成了4.6版本的，只是单独换了版本后，没见效。</p>

<h2>idea找不到 import&nbsp;project&nbsp;菜单。</h2>

<p>解决方法：打开idea的setting -&gt; 搜索gradle -&gt; Use local gradle distribution。 不然的话，我也不知道指向的是哪里的gradle，还是指定到自定义的固定的位置比较好。</p>

<p>使用IDEA准备导入项目时发现没有Import Project选项。。。</p>

<p>解决办法：</p>

<p>Settings &gt; Appearance &amp; Bechavior &gt; Menus and Toolbars.</p>

<p>打开Main menu &gt; File &gt; 选中File 下边任意一个, 点击 右侧按钮 Add After<br />
这里会弹出一个界面让你选择添加的功能</p>

<p>Import Project 选项在Other目录下， 找到import Project ，点击OK保存设置即可.<br />
&nbsp;</p>

<p>&nbsp;</p>
