<h1><a href="http://cmsblogs.com/?p=3839">【死磕 Spring】&mdash;&ndash; IOC 之 PropertyPlaceholderConfigurer 的应用</a></h1>

<p>在博客&nbsp;<a href="http://cmsblogs.com/?p=3837">【死磕 Spring】----- IOC 之 深入分析 PropertyPlaceholderConfigurer</a>&nbsp;中了解了 PropertyPlaceholderConfigurer 内部实现原理，她<strong>允许我们在 XML 配置文件中使用占位符并将这些占位符所代表的资源单独配置到简单的 properties 文件中来加载</strong>。这个特性非常重要，因为它我们对 Bean 实例属性的配置变得非常容易控制了，主要使用场景有：</p>

<ol>
	<li>动态加载配置文件，多环境切换</li>
	<li>属性加解密</li>
</ol>

<p>下面我们就第一个应用场景来做说明。</p>

<p><strong>利用 PropertyPlaceholderConfigurer 实现多环境切换</strong></p>

<p>在我们项目开发过程中，都会存在多个环境，如 dev 、test 、prod 等等，各个环境的配置都会不一样，在传统的开发过程中我们都是在进行打包的时候进行人工干预，或者将配置文件放在系统外部，加载的时候指定加载目录，这种方式容易出错，那么有没有一种比较好的方式来解决这种情况呢？有，<strong>利用 PropertyPlaceholderConfigurer 的特性来动态加载配置文件，实现多环境切换</strong>。</p>

<p>首先我们定义四个 Properties 文件，如下：</p>

<p><a href="https://gitee.com/chenssy/blog-home/raw/master/image/201811/15374242055683.jpg" onclick="return hs.expand(this);"><img alt="" src="https://gitee.com/chenssy/blog-home/raw/master/image/201811/15374242055683.jpg" /></a></p>

<p>内容如下：</p>

<pre>
<code>- application-dev.properties
student.name=chenssy-dev

- application-test.properties
student.name=chenssy-test

- application-prod.properties
student.name=chenssy-prod</code></pre>

<p>然后实现一个类，该类继承 PropertyPlaceholderConfigurer，实现&nbsp;<code>loadProperties()</code>，根据环境的不同加载不同的配置文件，如下：</p>

<pre>
<code>public class CustomPropertyConfig extends PropertyPlaceholderConfigurer {

    private Resource[] locations;

    private PropertiesPersister propertiesPersister = new DefaultPropertiesPersister();

    @Override
    public void setLocations(Resource[] locations) {
        this.locations = locations;
    }

    @Override
    public void setLocalOverride(boolean localOverride) {
        this.localOverride = localOverride;
    }

    /**
     * 覆盖这个方法，根据启动参数，动态读取配置文件
     * @param props
     * @throws IOException
     */
    @Override
    protected void loadProperties(Properties props) throws IOException {
        if (locations != null) {
            // locations 里面就已经包含了那三个定义的文件
            for (Resource location : this.locations) {
                InputStream is = null;
                try {
                    String filename = location.getFilename();
                    String env = &quot;application-&quot; + System.getProperty(&quot;spring.profiles.active&quot;, &quot;dev&quot;) + &quot;.properties&quot;;

                    // 找到我们需要的文件，加载
                    if (filename.contains(env)) {
                        logger.info(&quot;Loading properties file from &quot; + location);
                        is = location.getInputStream();
                        this.propertiesPersister.load(props, is);

                    }
                } catch (IOException ex) {
                    logger.info(&quot;读取配置文件失败.....&quot;);
                    throw ex;
                } finally {
                    if (is != null) {
                        is.close();
                    }
                }
            }
        }
    }
}</code></pre>

<p>配置文件：</p>

<pre>
<code>    &lt;bean id=&quot;PropertyPlaceholderConfigurer&quot; class=&quot;org.springframework.core.custom.CustomPropertyConfig&quot;&gt;
        &lt;property name=&quot;locations&quot;&gt;
            &lt;list&gt;
                &lt;value&gt;classpath:config/application-dev.properties&lt;/value&gt;
                &lt;value&gt;classpath:config/application-test.properties&lt;/value&gt;
                &lt;value&gt;classpath:config/application-prod.properties&lt;/value&gt;
            &lt;/list&gt;
        &lt;/property&gt;
    &lt;/bean&gt;

    &lt;bean id=&quot;studentService&quot; class=&quot;org.springframework.core.service.StudentService&quot;&gt;
        &lt;property name=&quot;name&quot; value=&quot;${student.name}&quot;/&gt;
    &lt;/bean&gt;</code></pre>

<p>在 idea 的 VM options 里面增加&nbsp;<code>-Dspring.profiles.active=dev</code>，标志当前环境为 dev 环境。测试代码如下：</p>

<pre>
<code>ApplicationContext context = new ClassPathXmlApplicationContext(&quot;spring.xml&quot;);

StudentService studentService = (StudentService) context.getBean(&quot;studentService&quot;);
System.out.println(&quot;student name:&quot; + studentService.getName());</code></pre>

<p>运行结果：</p>

<pre>
<code>student name:chenssy-dev</code></pre>

<p>当将&nbsp;<code>-Dspring.profiles.active</code>&nbsp;调整为 test，则打印结果则是 chenssy-test，这样就完全实现了根据不同的环境加载不同的配置，如果各位用过 Spring Boot 的话，这个就完全是 Spring Boot 里面的 profiles.active 。</p>

<p>PropertyPlaceholderConfigurer 对于属性的配置非常灵活，就看怎么玩了。</p>
